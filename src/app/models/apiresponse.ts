
import { ExtraParameters } from './property';

export interface ApiResponse {
  data: any[];
  message: string;
  otherParams: ExtraParameters,
  status: number,
  statusCode: number
};
