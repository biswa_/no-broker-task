export interface Property {
  data: any[];
  message: string;
  otherParams: ExtraParameters;
  status: number,
  statusCode: number
};

export interface ExtraParameters {
  count: number;
  city: string;
  region_id: string;
  total_count: string;
  lacality: string
};

export interface LocationObject {
  place_id: string;
};

export interface BreadcrumbData {
  city: string;
  region_id: string;
  total_count: number;
  locality: string,
  isReady: boolean
};
