export interface Filter {
  
};

export interface PriceRange {
  from: number;
  to: number;
}

export interface CurrentFilter {
  rent: string;
  amenities: string;
  bathroom: string;
  buildingType: string;
  furnishing: string;
  leaseType: string;
  parking: string;
  type: string
}