import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ApiResponse } from '../models/apiresponse';
import {AppSettings} from '../../environments/appsettings';

@Injectable()
export class PropertyService {
  constructor(private http: Http) { }

  getProperties(params: Object, location: string): Observable<ApiResponse> {
    let tempUrl = AppSettings.API_ENDPOINT + '/' + location;
    return this.http.get(tempUrl, {search: params})
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

}
