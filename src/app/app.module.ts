import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng4GeoautocompleteModule } from 'ng4-geoautocomplete';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { IonRangeSliderModule } from "ng2-ion-range-slider";
import { NgProgressModule, NgProgressBrowserXhr } from 'ngx-progressbar';


import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { PropertyService } from './services/property.service';
import { FilterComponent } from './components/filter/filter.component';
import { RightPanelComponent } from './components/right-panel/right-panel.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { CardComponent } from './components/card/card.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FilterComponent,
    RightPanelComponent,
    BreadcrumbComponent,
    CardComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    Ng4GeoautocompleteModule.forRoot(),
    InfiniteScrollModule,
    IonRangeSliderModule,
    NgProgressModule
  ],
  providers: [PropertyService, NgProgressBrowserXhr],
  bootstrap: [AppComponent]
})
export class AppModule { }
