import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { URLSearchParams } from '@angular/http';
import { NgProgress } from 'ngx-progressbar';

import { PropertyService } from './services/property.service';
import { ExtraParameters, LocationObject, BreadcrumbData } from './models/property';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [PropertyService]
})
export class AppComponent {
  public properties: any[] = [];
  public filterOpenState: boolean = false;
  public priceRange: Object = {
    'min': 0,
    'max': 100
  };

  private pageNo: number = 1;
  private filterParams: Object;
  private isLoadingApi: boolean;
  private currentLocation: string = 'ChIJLfyY2E4UrjsRVq4AjI7zgRY';
  public breadCrumbData: BreadcrumbData;

  constructor(private propertyService:PropertyService, public ngProgress: NgProgress) {
  }

  ngOnInit() {
    this.breadCrumbData = {
      city: '',
      region_id: '',
      total_count: 0,
      locality: 'HSR Layout',
      isReady: false
    };
    this.getProperties({});
  }

  onScroll () {
    if (!this.isLoadingApi && this.pageNo > 1) {
      this.getProperties(this.filterParams);
    }
  }

  getFilteredProperties(e:Object) {
    this.pageNo = 1;
    this.properties = [];
    this.getProperties(e);
  }

  getProperties (e:Object) {
    this.filterParams = e;
    let searchParams: URLSearchParams = new URLSearchParams();

    for (let key in e) {
      searchParams.set(key, e[key]);
    }
    if (this.pageNo > 1) {
      searchParams.set('pageNo', this.pageNo.toString());
    }
    this.isLoadingApi = true;
    this.ngProgress.start();
    this.breadCrumbData.isReady = false;
    this.propertyService.getProperties(searchParams, this.currentLocation).subscribe(prop => {
      this.ngProgress.done();
      this.isLoadingApi = false;
      this.properties = this.properties.concat(prop.data);
      this.breadCrumbData.city = prop.otherParams.city;
      this.breadCrumbData.region_id = prop.otherParams.region_id;
      this.breadCrumbData.total_count = parseInt(prop.otherParams.total_count);
      this.breadCrumbData.isReady = true;
      if (prop.otherParams.count >= 21) {
        this.pageNo++;
      } else {
        this.pageNo = 1;
      }
    });
  }

  headerEmits (event: Object) {
    switch (event['emitType']) {
      case 'locationSearch':
        this.pageNo = 1;
        this.properties = [];
        this.currentLocation = event['place_id'];
        this.getProperties(this.filterParams);
        this.breadCrumbData.locality = event['vicinity'];
        break;
      case 'toggleFilter':
        this.filterOpenState = event['state'];
        break;
      default:
        break;
    }
      
  }
}
