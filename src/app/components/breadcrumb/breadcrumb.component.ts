import { Component, OnInit } from '@angular/core';

import { BreadcrumbData } from '../../models/property';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
  inputs: ['breadCrumbData']
})
export class BreadcrumbComponent implements OnInit {

  public breadCrumbData:BreadcrumbData = this.breadCrumbData;

  constructor() { }

  ngOnInit() {
  }

}
