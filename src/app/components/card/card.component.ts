import { Component, OnInit, Input } from '@angular/core';
import { DatePipe, LowerCasePipe } from '@angular/common';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() property: any;

  constructor() { }

  ngOnInit() {
    this.property.photo = 'https://assets.nobroker.in/static/img/nopic_1bhk.jpg';
    // if (!this.property.photos.length) {
    //   this.property.photo = 'https://assets.nobroker.in/static/img/nopic_1bhk.jpg';
    // } else {
    //   this.property.photo = 'https://d3snwcirvb4r88.cloudfront.net/images/' + this.property.photos[0].imagesMap.large;
    // }
  }
}
