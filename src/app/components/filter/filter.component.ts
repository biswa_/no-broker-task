import { Component, OnInit, EventEmitter } from '@angular/core';


import { PriceRange, CurrentFilter } from '../../models/filter';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
  outputs: ['filterOutput'],
  inputs: ['openState']
})
export class FilterComponent implements OnInit {
  public apartMentTypes: any[];
  public preferredTenants: any[];
  public furnishing: any[];
  public propertyTypes: any[];
  public parking: any[];
  public bathrooms: any[];
  public amenities: any[];
  public leaseOnly: boolean;
  public advancedFilter: boolean = false;
  public advancedFilterLength: number;
  public priceRange: PriceRange;
  public advancedFilterMap: string[];
  public openState:boolean = this.openState;
  public filterParams: Object;

  private currentFilter: CurrentFilter;

  public init () {
    this.apartMentTypes = [
      {key: '1 RK', value: 'RK1'},
      {key: '1 BHK', value: 'BHK1'},
      {key: '2 BHK', value: 'BHK2'},
      {key: '3 BHK', value: 'BHK3'},
      {key: '4 BHK', value: 'BHK4'},
      {key: '4+ BHK', value: 'BHK4PLUS'}
    ];

    this.preferredTenants = [
      {key: 'Family', value: 'FAMILY'},
      {key: 'Bachelor', value: 'BACHELOR'},
      {key: 'Company', value: 'COMPANY'}
    ];

    this.furnishing = [
      {key: 'Full', value: 'FULLY_FURNISHED'},
      {key: 'Semi', value: 'SEMI_FURNISHED'},
      {key: 'No', value: 'NOT_FURNISHED'}
    ];

    this.propertyTypes = [
      {key: 'Apartment', value: 'AP'},
      {key: 'Independent House/Villa', value: 'IH'},
      {key: 'Independent Floor/Builder Floor', value: 'IF'}
    ];

    this.parking = [
      {key: '2 Wheeler', value: 'TWO_WHEELER'},
      {key: '4 Wheeler', value: 'FOUR_WHEELER'}
    ];

    this.bathrooms = [
      {key: '1 or more', value: 1},
      {key: '2 or more', value: 2},
      {key: '3 or more', value: 3}
    ];

    this.amenities = [
      {key: 'Gym'},
      {key: 'Lift'},
      {key: 'Swimming Pool'}
    ];
    this.advancedFilterLength = 0;
    this.priceRange = {
      to: 50000,
      from: 0
    };
    this.advancedFilterMap = [
      'buildingType',
      'parking',
      'bathroom',
      'amenities'
    ];
    this.filterParams = {
      furnishing: [],
      leaseType: [],
      buildingType: [],
      parking: [],
      amenities: [],
      bathroom: [],
      type: []
    };
    this.currentFilter = {
      rent: '',
      amenities: '',
      bathroom: '',
      buildingType: '',
      furnishing: '',
      leaseType: '',
      parking: '',
      type: ''
    };
  }

  constructor() {
  }

  ngOnInit() {
    this.init();
  }

  compileFilterParams (list, type) {
    let obj: Object = {};
    let index: number;

    if (list === 'undefined' || type === 'undefined') {
      return obj;
    }
    if (!list.key) {
      obj[type] = list;
    } else {
      index = this.filterParams[type] ? this.filterParams[type].indexOf(list.value || list.key) : null;
      if (index === -1 && list.selected) {
        this.filterParams[type].push(list.value || list.key);
      } else {
        this.filterParams[type].splice(index, 1);
      }
      if (list.selected === undefined) {
        this.filterParams[type] = [list.value || list.key];
      }
    }

    for (let key in this.filterParams) {
      if (this.filterParams[key].length) {
        obj[key] = this.filterParams[key].join(',');
      }
    }
    return obj;
  }

  filterOutput = new EventEmitter<Object>();

  filterValueChanged(list, type) {
    let key;
    this.advancedFilterLength = 0;

    if (type === 'leaseOnly' && list) {
      list = 1;
    } else if (type === 'leaseOnly' && !list) {
      list = 0;
    }
    let params = this.compileFilterParams(list, type);
    for (key in params) {
       this.currentFilter[key] = params[key];

      if ((typeof params[key] === 'string' || params[key] instanceof String) && this.advancedFilterMap.indexOf(key) !== -1) {
        this.advancedFilterLength += params[key].split(',').length;
      }
    }
    this.filterOutput.emit(params);
  }

  toggleFilter () {
    this.advancedFilter = !this.advancedFilter;
  }

  priceRangeChange (event) {
    this.priceRange.to = event.to;
    this.priceRange.from = event.from;
    this.currentFilter.rent = event.from + ',' + event.to;
    this.filterOutput.emit(this.currentFilter);
  }

  resetFilter () {
    if (!Object.keys(this.currentFilter).length) {
      return;
    }
    this.init();
    this.filterOutput.emit({});
  }

}
