import { Component, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  outputs: ['headerDataEmitter']
})
export class HeaderComponent implements OnInit {
  private filterOpenState: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  headerDataEmitter = new EventEmitter<Object>();

  placesSearchCallback(selectedData:Object) {
    if (selectedData) {
      selectedData['emitType'] = 'locationSearch';
      this.headerDataEmitter.emit(selectedData);
    }
  }

  toggleFilter () {
    this.filterOpenState = !this.filterOpenState;
    this.headerDataEmitter.emit({state: this.filterOpenState, emitType: 'toggleFilter'});
  }

}
